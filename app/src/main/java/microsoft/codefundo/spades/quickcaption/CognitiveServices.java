package microsoft.codefundo.spades.quickcaption;

/**
 * Created by avishek on 3/5/17.
 */

import android.net.Uri;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
//import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
//import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.net.URI;
import java.net.URL;

public class CognitiveServices
{
    public static void main(String[] args)
    {
        HttpClient httpclient = new DefaultHttpClient();

        try
        {
            //"https://westus.api.cognitive.microsoft.com/vision/v1.0/analyze"

            //builder.setParameter("visualFeatures", "Categories");
            //builder.setParameter("details", "{string}");
            //builder.setParameter("language", "en");

            //URI uri = builder.build();
            HttpPost request = new HttpPost(Config.MIC_COG_SER_URL);
            request.setHeader("Content-Type", "application/json");
            request.setHeader("Ocp-Apim-Subscription-Key", "{1455b75b6c7e46f3a2b212f647f38424}");


            // Request body
            StringEntity reqEntity = new StringEntity("{\"url\":\"http://phenomena.nationalgeographic.com/files/2013/01/giraffes-standoff.jpg\"}");
            request.setEntity(reqEntity);

            HttpResponse response = httpclient.execute(request);
            HttpEntity entity = response.getEntity();

            if (entity != null)
            {
                System.out.println(EntityUtils.toString(entity));
            }
        }
        catch (Exception e)
        {
            System.out.println(e.getMessage());
        }
    }
}