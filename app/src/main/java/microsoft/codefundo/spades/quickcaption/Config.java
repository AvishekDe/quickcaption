package microsoft.codefundo.spades.quickcaption;

/**
 * Created by avishek on 3/4/17.
 */

public class Config {
    // File upload url (replace the ip with your server address)
    public static final String FILE_UPLOAD_URL = "http://10.42.0.1:5000/upload"; //LOCALHOST
    //public static final String FILE_UPLOAD_URL = "http://40.71.196.100:8000/upload"; // AZURE
    public static final String MIC_COG_SER_URL = "https://westus.api.cognitive.microsoft.com/vision/v1.0/analyze";
    // Directory name to store captured images and videos
    public static final String IMAGE_DIRECTORY_NAME = "Codefundo";
}